#!/bin/sh
# @vasm: vbootset
# @level: root
# @description: select default run level for booting
# 
# (c) Kocil, 2003
# (c) Vector Linux, 2003

VDIR=`dirname $0`
. $VDIR/vasm-functions

check_root

BOOT_MODE="2"
USER_ID=1000
NEXT_LEVEL=4

fpasswd="/etc/passwd"
finit=/etc/inittab
fauto=/etc/rc.d/rc.A

get_bootmode()
{
   LINE=`grep -e "^id:[0-9]:initdefault:" $finit`
   dbug boot_line $LINE
   if [ "$LINE" ]; then
      BOOT_MODE=`echo $LINE | cut -d : -f 2`         
   fi
   dbug boot_mode $BOOT_MODE
}

set_bootmode()
{
   cat $finit \
   | sed "s/^id:[0-9]:initdefault:/id:$BOOT_MODE:initdefault:/" \
   > $finit.new
   mv -f $finit.new $finit
}

set_autouser()
{
   if [ "$USER_ID" ]; then    
      cat $fauto | sed "
      s/^USER_ID=.*/USER_ID='$USER_ID'/
      s/^NEXT_LEVEL=.*/NEXT_LEVEL='$NEXT_LEVEL'/" > $fauto.new
      mv -f $fauto.new $fauto
      chmod +x $fauto
   fi
}

boot_status()
{
   if [ "$1" = "$BOOT_MODE" ]; then
      echo "on"
   else
      echo "off"
   fi
}

###################################################
menu_mode()
{

TITLE="BOOT MODE CONFIGURATOR"
TEXT="\n
This menu allows you to choose the default boot mode.\n
Mind that you may choose the graphical mode (4/5)\n
if X-Windows has been setup properly.\n
Use [space bar] to select the mode, then press OK."
DIMENSION="16 60 5"

$DCMD --backtitle "$BACKTITLE" --title "$TITLE" --radiolist \
"$TEXT" $DIMENSION \
2 "Text user interface desktop" $(boot_status 2) \
3 "Text user interface server" $(boot_status 3) \
4 "Graphical user interface desktop" $(boot_status 4) \
5 "Graphical user interface server" $(boot_status 5) \
2> $freply || return $?
# 
# A "Auto login with GUI desktop" $(boot_status 8) \

BOOT_MODE=`cat $freply`

case $BOOT_MODE in
  2|3|4|5)
     infobox "Setting up boot mode ..."
     set_bootmode
     sleep 2
     clean_exit 0
     ;;
  A)
     BOOT_MODE=8
     ;;
esac
return 0
}


menu_user () {

echo '$DCMD --backtitle "$BACKTITLE" --title "$TITLE" --menu "$TEXT" $DIMENSION \' > $fmenu
echo '"EDIT" "Enter name manually" \' >> $fmenu
## List the user, but only for the first 100
awk -F: '{ if ($3>=500) {print $0; num++}; if (num>=100) exit 0}' $fpasswd \
   | sort | while read LINE; do
   N1=`echo $LINE | cut -d ':' -f 1`
   N2=`echo $LINE | cut -d ':' -f 5 | cut -d ',' -f 1`
   echo "$N1" "\"$N2 \" \\" >> $fmenu
done
echo '2> $freply' >> $fmenu

cat $fmenu
while [ 1 ]; do
  DIMENSION="20 58 8"
  TITLE="AUTOLOGIN USER"
  TEXT="\n
Autologin allows your computer to boot directly into
a graphical desktop without login procedure. This is
convenient, but not secure. Unless you are very sure,
do not activate this mode.\n
Hit <Cancel> now, or select a user for autologin:\n"

  source $fmenu

  result=$?
  [ $result != 0 ] && clean_exit $result

  USER_NAME=`cat $freply`

  if [ "$USER_NAME" = "EDIT" ]; then
     USER_NAME=""
     while [ -z "$USER_NAME" ]; do
       inputbox "Enter user name:" "AUTOLOGIN USER"
       result=$?
       [ $result != 0 ] && return $result
       USER_NAME=`cat $freply`
     done
  fi
  # check user existence
  USER_LINE=`grep -e "^${USER_NAME}:" $fpasswd`
  if [ "$USER_LINE" ]; then
    USER_ID=`echo $USER_LINE | cut -f3 -d:`
    return 0
  fi
  retrybox "User '$USER_NAME' does not exist."
done
}

## This is supposed to set the services for auto login
## For now, lets just copy the service from Level 4
menu_service()
{
    infobox "Setting up autologin ..."
    (
       cd /etc/rc.d;
       if [ ! -h rc8.d ] ; then
          rm -rf rc8.d
	  ln -s rc5.d rc8.d
       fi
       if [ ! -h rc7.d ] ; then
          rm -rf rc7.d
	  ln -s rc4.d rc7.d
       fi
    )           
    set_bootmode
    set_autouser
    sleep 1
    clean_exit 0
}

########################################################
# MAIN PROGRAM
dbug "VBOOT"

get_bootmode
wizard menu_mode menu_user menu_service
clean_exit 0
